FROM thaalesalves/centos
LABEL Created by Thales Alves <thales@thalesalv.es>

#######################################################
### Environment
#######################################################
USER root
EXPOSE 5432 22

RUN echo 'alias ls="ls --block-size=M"' >> /etc/bashrc
RUN echo 'alias ll="ls -la"' >> /etc/bashrc
RUN echo 'alias l="ls -l"' >> /etc/bashrc
RUN echo 'alias la="ls -a"' >> /etc/bashrc

RUN localedef -i pt_BR -f UTF-8 pt_BR.UTF-8
ENV LANG pt_BR.UTF-8
ENV LANGUAGE pt_BR.UTF-8
ENV LC_CTYPE UTF-8
ENV LC_COLLATE C

RUN yum update -y
RUN yum upgrade -y
RUN yum install -y nc> /dev/null 2>&1
RUN yum install -y which> /dev/null 2>&1
RUN yum install -y telnet mc git emacs most screen curl wget nano
RUN yum install -y system-config-language openssl sudo
RUN yum install -y unzip which tar more util-linux-ng passwd openssh-clients openssh-server ed m4
RUN yum clean all
RUN mkdir /temp-files
RUN echo root:root | chpasswd

#######################################################
### GPDB Installation
#######################################################
ADD packages/greenplum-db-4.3.7.1-build-1-RHEL5-x86_64.zip /temp-files
RUN unzip /temp-files/greenplum-db-4.3.7.1-build-1-RHEL5-x86_64.zip -d /temp-files

RUN rm /temp-files/greenplum-db-4.3.7.1-build-1-RHEL5-x86_64.zip
RUN sed -i s/"more << EOF"/"cat << EOF"/g /temp-files/greenplum-db-4.3.7.1-build-1-RHEL5-x86_64.bin 
RUN echo -e "yes\n\nyes\nyes\n" | /temp-files/greenplum-db-4.3.7.1-build-1-RHEL5-x86_64.bin 
RUN rm /temp-files/greenplum-db-4.3.7.1-build-1-RHEL5-x86_64.bin 
RUN cat /temp-files/sysctl.conf.add >> /etc/sysctl.conf 
RUN cat /temp-files/limits.conf.add >> /etc/security/limits.conf
RUN rm -f /temp-files/*.add
RUN echo "localhost" > /temp-files/gpdb-hosts
RUN chmod 777 /temp-files/gpinitsystem_singlenode
RUN hostname > ~/orig_hostname
RUN mv /temp-files/run.sh /usr/local/bin/run.sh
RUN chmod +x /usr/local/bin/run.sh
RUN /usr/sbin/groupadd gpadmin
RUN /usr/sbin/useradd gpadmin -g gpadmin -G wheel
RUN echo gpadmin:gpadmin | chpasswd
RUN echo "gpadmin        ALL=(ALL)       NOPASSWD: ALL" >> /etc/sudoers
RUN mv /temp-files/bash_profile /home/gpadmin/.bash_profile
RUN chown -R gpadmin: /home/gpadmin
RUN mkdir -p /gpdata/master /gpdata/segments
RUN chown -R gpadmin: /gpdata
RUN chown -R gpadmin: /usr/local/green*
RUN service sshd start
RUN su gpadmin -l -c "source /usr/local/greenplum-db/greenplum_path.sh;gpssh-exkeys -f /temp-files/gpdb-hosts"
RUN su gpadmin -l -c "source /usr/local/greenplum-db/greenplum_path.sh;gpinitsystem -a -c  /temp-files/gpinitsystem_singlenode -h /temp-files/gpdb-hosts; exit 0 "
RUN su gpadmin -l -c "export MASTER_DATA_DIRECTORY=/gpdata/master/gpseg-1;source /usr/local/greenplum-db/greenplum_path.sh;psql -d template1 -c \"alter user gpadmin password 'secret'\"; createdb gpadmin;  exit 0"
RUN su gpadmin -l -c "/usr/local/bin/run.sh"

RUN mv /temp-files/scripts/entrypoint.sh
ENTRYPOINT [ "/entrypoint.sh" ]